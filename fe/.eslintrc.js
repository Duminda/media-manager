module.exports =  {
  parser:  '@typescript-eslint/parser',
  "plugins": [
    "react-hooks",
    "@typescript-eslint",
  ],
  extends:  [
    'plugin:react/recommended',  
    'plugin:@typescript-eslint/recommended', 
    "prettier/@typescript-eslint", // Uses eslint-config-prettier to disable ESLint rules from @typescript-eslint/eslint-plugin that would conflict with prettier
    "plugin:prettier/recommended" 
  ],
  parserOptions:  {
    ecmaVersion:  2018,  
    sourceType:  'module',  
    ecmaFeatures:  {
      jsx:  true,  
    },
  },
  rules:  {
    '@typescript-eslint/no-var-requires': 0,
    "no-var": 0,
    "prefer-rest-params": 0,
    "@typescript-eslint/no-empty-function": 0,
    "no-unused-vars": "off",
    "no-undef": "off",
    "no-use-before-define": "off",
    "jsx-a11y/href-no-hash": "off",
    "react-hooks/rules-of-hooks": "error",
    "react-hooks/exhaustive-deps": "warn",
    "@typescript-eslint/no-unused-vars": "error",
    "@typescript-eslint/no-use-before-define": "off",
    "@typescript-eslint/explicit-function-return-type": "off",
    "@typescript-eslint/camelcase": "off",
    "@typescript-eslint/explicit-member-accessibility": "off",
    "@typescript-eslint/ban-ts-ignore": "off"
  },
  settings:  {
    react:  {
      version:  'detect', 
    },
  },
};
