const { merge } = require('webpack-merge');
const commonConfig = require('./webpack.config.common.js');
var path = require('path');
const Dotenv = require('dotenv-webpack');

module.exports = merge(commonConfig, {
  mode: 'development',
  target: 'web',
  output: {
    filename: 'bundle-dev.js',
    path: path.resolve(__dirname, '..', 'public'),
    publicPath: '/',
  },
  entry: {
    app: path.join(__dirname, 'src', 'index.tsx'),
  },
  devServer: {
    port: 3001,
    proxy: {
        '/api': {
            'target': 'http://127.0.0.1:3000',
        },
        pathRewrite: {
          '^/api': ''
       }
    },
    historyApiFallback: true,
  },
  devtool: 'source-map',
  plugins: [
    new Dotenv({
      path: './dev.env',
    }),
  ],
});
