import axios, { AxiosResponse } from 'axios';

const login: any = async (username: string, password: string) => {
    

    const requestBody = {
        username: username,
        password: password
    };

    try {

        const loginData =  await axios.post(process.env.AUTH_LOGIN_URL, requestBody);

        return loginData.data.token;
    }catch(error)
    {
        throw new Error('Unable to get a token.')
    }        
};

export const authService = {
    login
};
