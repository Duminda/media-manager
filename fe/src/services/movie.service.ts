import axios, { AxiosResponse } from 'axios';

interface Movie {
    id: number;
    title: string;
    description: string;
    thumbnail: string;
    releasedDate: string;
}

const getAllMovies: any = async (token: string) => {

    const config = {
        headers: {
            Authorization: `Bearer ${token}`,
        },
    };

    try {
        const movies = await axios.get(process.env.MOVIE_URL, config);
        return await movies;
    } catch (error) {
        console.log(error);

        return error;
    }
};

const getMovie: any = async (token: string, id : number) => {

    const config = {
        headers: {
            Authorization: `Bearer ${token}`,
        },
    };

    try {
        const movie = await axios.get(process.env.MOVIE_URL + id, config);
        return await movie;
    } catch (error) {
        console.log(error);

        return error;
    }
};

const addMovie: any = async (token: string,  movie: Movie) => {
    const config = {
        headers: {
            Authorization: `Bearer ${token}`,
        },
    };

    try {
        const newMovie = await axios.post(process.env.MOVIE_URL, movie, config);
        return await newMovie;
    }catch (error) {
        console.log(error);

        return error;
    }
}

const updateMovie: any = async (token: string, movie: Movie) => {
    const config = {
        headers: {
            Authorization: `Bearer ${token}`,
        },
    };

    try {
        const updatedMovie = await axios.put(process.env.MOVIE_URL, movie, config);
        return await updatedMovie;
    }catch (error) {
        console.log(error);

        return error;
    }
}

const deleteMovie: any = async (token: string, id: number) => {
    const config = {
        headers: {
            Authorization: `Bearer ${token}`,
        },
    };

    try {
        const deletedMovie = await axios.delete(process.env.MOVIE_URL + id, config);
        return await deletedMovie;
    }catch (error) {
        console.log(error);

        return error;
    }
}


const bulkUpdate: any = async (token: string) => {
    const config = {
        headers: {
            Authorization: `Bearer ${token}`,
        },
    };

    try {
        const updatedMovies = await axios.post(process.env.MOVIE_URL + "bulk", {}, config);
        return await updatedMovies;
    }catch (error) {
        console.log(error);

        return error;
    }
}

export const movieService = {
    getAllMovies,
    getMovie,
    addMovie,
    updateMovie,
    deleteMovie,
    bulkUpdate
};
