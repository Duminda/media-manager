import * as React from 'react';
import { authContext, IAuth } from './context'
//import { authService} from '../services/auth.service'

export const AuthProvider: React.FC<{
   
}> = ({ children }) => {
    const auth : any = useProvideAuth();
    return ( <authContext.Provider value={auth}>{ children }</authContext.Provider>)
}

const useProvideAuth : any = () =>{
    return  localStorage.getItem("access_token") !== null
}

export const useAuth = () => {
    return React.useContext(authContext)
}