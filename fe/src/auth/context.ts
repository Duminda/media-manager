import { createContext} from 'react';

export interface IAuth {
    loggedIn: boolean
}

export const authContext = createContext<IAuth>(false)
