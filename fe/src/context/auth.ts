import { createContext, useContext } from 'react';

export const AuthContext: any = createContext('auth');

export function useAuth(): typeof AuthContext {
    return useContext(AuthContext);
}
