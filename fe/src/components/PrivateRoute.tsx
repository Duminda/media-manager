import * as React from 'react';
import { Route, Navigate, Outlet } from 'react-router-dom';
import * as PropTypes from 'prop-types';
import { useAuth } from '../auth/auth'

const PrivateRoute: React.FC<{
    children: any;
    redirectTo: any;
}> = ({children , redirectTo }) => {

    const auth = useAuth();

    return auth ? children : <Navigate to={redirectTo} />;
};
export default PrivateRoute;

PrivateRoute.propTypes = {
    redirectTo: PropTypes.any,
    children: PropTypes.any
};
