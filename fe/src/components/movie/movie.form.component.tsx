import * as React from 'react';
//import {useParams} from 'react-router-dom'

import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';

import { movieService } from '../../services/movie.service';
import NavHeader from '../header/header.component';

const formReducer = (state: any, event: any) => {
    return {
        ...state,
        [event.name]: event.value,
    };
};

interface Movie {
    id: number;
    title: string;
    description: string;
    thumbnail: string;
    releasedDate: string;
}

const MovieForm: React.FC<{}> = (props) => {

    const [movieFormData, setFormdata] = React.useReducer(formReducer, {});
    const [movieUpdated, setMovieUpdated] = React.useState(null);
    const [movieBulkUpdated, setMoviesBulkUpdated] = React.useState(null);
    const [movieAdded, setMovieAdded] = React.useState(null);
   

   /* const {id} = useParams();
    React.useEffect(() => {
        console.log('get Movie...');

        const token = localStorage.getItem('access_token');

        movieService.getMovie(token, id).then((movies: any) => {
            console.log(movies.data);
            setMovieAdded(movies.data);
        });
    }, []);*/

    const handleChange = (event: any) => {
        setFormdata({
            name: event.currentTarget.name,
            value: event.currentTarget.value,
        });
    };

    const handleMovieAdd = async (event: React.SyntheticEvent) => {
        event.preventDefault();

        const token = localStorage.getItem('access_token');

        try {
            const newMovie = await movieService.addMovie(token, movieFormData);
            setMovieAdded(newMovie);
       
        } catch (error) {
            console.log(error);
        }
    };

    const handleMovieUpdate = async (event: React.SyntheticEvent) => {
        event.preventDefault();

        const token = localStorage.getItem('access_token');

        try {
            const updatedMovie = await movieService.updateMovie(token, movieFormData);
            setMovieUpdated(updatedMovie);
       
        } catch (error) {
            console.log(error);
        }
    };

    const handleMovieBulkUpdate = async (event: React.SyntheticEvent) => {
        event.preventDefault();

        const token = localStorage.getItem('access_token');

        try {
            const updatedMovies = await movieService.bulkUpdate(token);
            setMoviesBulkUpdated(updatedMovies);
       
        } catch (error) {
            console.log(error);
        }
    };

    return (
        <>
            <NavHeader />
            <Container component="main" maxWidth="xs">
                    <Box
                        sx={{
                            marginTop: 8,
                            display: 'flex',
                            flexDirection: 'column',
                            alignItems: 'center',
                        }}
                    >
                        <Typography component="h1" variant="h5">
                            Movie Operations
                        </Typography>
                        <Box component="form" noValidate sx={{ mt: 1 }}>
                            <TextField
                                margin="normal"
                                required
                                fullWidth
                                id="title"
                                label="Title"
                                name="title"
                                autoComplete="title"
                                autoFocus
                                onChange={handleChange}
                            />
                            <TextField
                                margin="normal"
                                required
                                fullWidth
                                name="description"
                                label="Description"
                                id="description"
                                onChange={handleChange}
                            />
                            <Button onClick={handleMovieAdd}  fullWidth variant="contained" sx={{ mt: 3, mb: 2 }}>
                                Add Movie
                            </Button>
                            <Button  onClick={handleMovieUpdate} fullWidth variant="contained" sx={{ mt: 3, mb: 2 }}>
                                Update Movie
                            </Button> 

                            <Button  onClick={handleMovieBulkUpdate} fullWidth variant="contained" sx={{ mt: 3, mb: 2 }}>
                                Bulk Update
                            </Button> 
                        </Box>
                    </Box>
                </Container>
        </>
    )

}

export default MovieForm;