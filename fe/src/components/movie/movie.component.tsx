import * as React from 'react';

import { movieService } from '../../services/movie.service';
import NavHeader from '../header/header.component';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Button from '@mui/material/Button';

const Movies: React.FC<{}> = (props) => {
    const [movies, setMovies] = React.useState([]);

    React.useEffect(() => {
        console.log('get Movies...');

        const token = localStorage.getItem('access_token');

        movieService.getAllMovies(token).then((movies: any) => {
            console.log(movies.data);
            setMovies(movies.data);
        });
    }, []);

    const handleMovieDelete = async (event: React.SyntheticEvent, id: number) => {
        event.preventDefault();

        const token = localStorage.getItem('access_token');

        const deleted = await movieService.deleteMovie(token, id);
    }

    return (
        <>
            <NavHeader />
            <TableContainer>
                <Table sx={{ maxWidth: 650, marginTop: 5 }} size="small" aria-label="a dense table">
                    <TableHead>
                        <TableRow>
                            <TableCell>Movie Name</TableCell>
                            <TableCell align="right">Delete</TableCell>
                            <TableCell align="right">Update</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {movies.map((movie: any) => (
                            <TableRow key={movie.id} sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                                <TableCell align="left">{movie.title}</TableCell>
                                <TableCell align="right">
                                    <Button size="small" onClick={(e)=>{handleMovieDelete(e, movie.id)}} variant="outlined" color="error" >
                                        Delete
                                    </Button>
                                </TableCell>
                                <TableCell align="right">
                                    <Button size="small" variant="outlined" href={`/update`}>
                                        Update
                                    </Button>
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </>
    );
};

export default Movies;
