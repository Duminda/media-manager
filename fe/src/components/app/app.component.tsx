import * as React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Login from '../login/login.component';
import PrivateRoute from '../PrivateRoute';
import Movies from '../movie/movie.component';
import MovieForm from '../movie/movie.form.component';
import { AuthProvider} from '../../auth/auth'


const App: React.FC = () => {
  
    return (
        <AuthProvider>
            <Router>
                <Routes>
                    <Route path="/" element={<Login/>} />                    
                    <Route path='/movies' element={<Movies/>}/>
                    <Route path='/update' element={<PrivateRoute redirectTo="/login" ><MovieForm/> </PrivateRoute>}/>
                    <Route path="/logout" element={<Login/>}>
                    </Route>
                </Routes>
            </Router>
        </AuthProvider>
    );
};

export default App;
