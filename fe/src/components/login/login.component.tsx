import * as React from 'react';

import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';
import { Navigate } from 'react-router-dom';

import { authService } from '../../services/auth.service';

const formReducer = (state: any, event: any) => {
    return {
        ...state,
        [event.name]: event.value,
    };
};

const Login: React.FC<{}> = (props) => {
    const [loginFormData, setFormdata] = React.useReducer(formReducer, {});
    const [token, setToken] = React.useState(null);

    const handleSubmit = async (event: React.SyntheticEvent) => {
        event.preventDefault();

        try {
            const token = await authService.login(loginFormData.username, loginFormData.password);

            setToken(token);
            localStorage.setItem('access_token', token);
        } catch (error) {
            console.log(error);
        }
    };

    const handleChange = (event: any) => {
        setFormdata({
            name: event.currentTarget.name,
            value: event.currentTarget.value,
        });
    };

    if (token) return <Navigate to="/movies" state={token} />;
    else
        return (
            <div>
                <header>&nbsp;</header>
                <Container component="main" maxWidth="xs">
                    <Box
                        sx={{
                            marginTop: 8,
                            display: 'flex',
                            flexDirection: 'column',
                            alignItems: 'center',
                        }}
                    >
                        <Typography component="h1" variant="h5">
                            Sign in
                        </Typography>
                        <Box component="form" onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}>
                            <TextField
                                margin="normal"
                                required
                                fullWidth
                                id="username"
                                label="Username"
                                name="username"
                                autoComplete="username"
                                autoFocus
                                onChange={handleChange}
                            />
                            <TextField
                                margin="normal"
                                required
                                fullWidth
                                name="password"
                                label="Password"
                                type="password"
                                id="password"
                                autoComplete="current-password"
                                onChange={handleChange}
                            />
                            <Button type="submit" fullWidth variant="contained" sx={{ mt: 3, mb: 2 }}>
                                Sign In
                            </Button>
                            <Grid container>
                                <Grid item>
                                    <Link href="#" variant="body2">
                                        {'Not registered ? Sign Up'}
                                    </Link>
                                </Grid>
                            </Grid>
                        </Box>
                    </Box>
                </Container>
            </div>
        );
};

export default Login;
