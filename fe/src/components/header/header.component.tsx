import * as React from 'react';
import { NavLink } from 'react-router-dom';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';

const NavHeader: React.FC = () => {
    return (
        <header>
            <AppBar position="static">
                <Container maxWidth="xl">
                    <Toolbar disableGutters>
                        <Typography
                            variant="h6"
                            noWrap
                            component="div"
                            sx={{ mr: 2, display: { xs: 'none', md: 'flex' } }}
                        >
                            Movie Manager
                        </Typography>
                        <NavLink to="/movies">Home</NavLink>
                        &nbsp;&nbsp;
                        <NavLink to="/">Logout</NavLink>
                    </Toolbar>
                </Container>
            </AppBar>
        </header>
    );
};

export default NavHeader;
