const commonConfig = require('./webpack.config.common.js');
const webpack = require('webpack');
const { merge } = require('webpack-merge');
var path = require('path');
const Dotenv = require('dotenv-webpack');

module.exports = (env) => {
  return merge(commonConfig, {
    output: {
        path: path.join(__dirname,'dist'),
        filename: "[name].js",
        publicPath: '/',
    },
    mode: 'production',
    optimization: {
      minimize: true,
    },
    plugins: [
      new webpack.DefinePlugin({
        'process.env.NODE_ENV': JSON.stringify('production'),
      }),
      new Dotenv({
        path: './prod.env',
      }),
    ],
  });
};
