
### Runing app

```bash
docker-compose up -d

```

App Url : http://localhost

### Application User

```bash
user: demouser  
pass: abc123 
```

#### Commands used in docker container

```bash

npx sequelize-cli db:migrate

npx sequelize-cli db:seed:all

npm start
```
### APIs

```bash
$ http POST http://localhost:3000/auth/register username=duminda password=abc123
HTTP/1.1 200 OK
Connection: keep-alive
Content-Length: 37
Content-Type: application/json; charset=utf-8
Date: Thu, 16 Dec 2021 18:35:54 GMT
ETag: W/"25-18OjcpUEMiVZVBGsWtptK5Nkn4g"
Keep-Alive: timeout=5
Vary: Origin
X-Powered-By: Express

{
    "message": "Registration successful"
}
```

```bash
$ http POST http://localhost:3000/auth/login username=duminda password=abc123
HTTP/1.1 200 OK
Connection: keep-alive
Content-Length: 131
Content-Type: application/json; charset=utf-8
Date: Thu, 16 Dec 2021 18:36:35 GMT
ETag: W/"83-/bHrEWDaUI8gxZcKjYdK4Bl04g4"
Keep-Alive: timeout=5
Vary: Origin
X-Powered-By: Express

{
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7fSwiaWF0IjoxNjM5Njc5Nzk1fQ.cBoFNqinhtjsuT-91YJtPDkk_N2W7W-Ht_vTs18JeDg"
}
```
```bash
$ http POST http://localhost/api/movies/bulk 'Authorization:Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7fSwiaWF0IjoxNjM5Njc5Nzk1fQ.cBoFNqinhtjsuT-91YJtPDkk_N2W7W-Ht_vTs18JeDg'
HTTP/1.1 200 OK
Connection: keep-alive
Content-Length: 66
Content-Type: application/json; charset=utf-8
Date: Fri, 17 Dec 2021 19:37:53 GMT
ETag: W/"42-QhsDxtpcJttSW/cGvnpxkBVYmZA"
Server: nginx/1.21.4
Vary: Origin
X-Powered-By: Express

{
    "count": 0,
    "message": "Movies Inserted/Updated",
    "status": "SUCCESS"
}
```