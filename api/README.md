npx sequelize-cli init

npx sequelize-cli model:generate --name Movie --attributes title:string,description:string

npx sequelize-cli seed:generate --name demo-user

npx sequelize-cli db:migrate

npm start

duminda@drw:~/js-workspace/movie-manager-frontend$ http POST http://localhost:3000/auth/register username=duminda password=abc123
HTTP/1.1 200 OK
Connection: keep-alive
Content-Length: 37
Content-Type: application/json; charset=utf-8
Date: Thu, 16 Dec 2021 18:35:54 GMT
ETag: W/"25-18OjcpUEMiVZVBGsWtptK5Nkn4g"
Keep-Alive: timeout=5
Vary: Origin
X-Powered-By: Express

{
    "message": "Registration successful"
}

duminda@drw:~/js-workspace/movie-manager-frontend$ http POST http://localhost:3000/auth/login username=duminda password=abc123
HTTP/1.1 200 OK
Connection: keep-alive
Content-Length: 131
Content-Type: application/json; charset=utf-8
Date: Thu, 16 Dec 2021 18:36:35 GMT
ETag: W/"83-/bHrEWDaUI8gxZcKjYdK4Bl04g4"
Keep-Alive: timeout=5
Vary: Origin
X-Powered-By: Express

{
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7fSwiaWF0IjoxNjM5Njc5Nzk1fQ.cBoFNqinhtjsuT-91YJtPDkk_N2W7W-Ht_vTs18JeDg"
}
