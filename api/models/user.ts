import { Model, DataTypes } from "sequelize";
import bcrypt from 'bcrypt'
import passport from 'passport'
import jwt from 'jsonwebtoken';
import passportLocal from 'passport-local'
import passportJwt from 'passport-jwt'

import DBConfig from './'

const sequelize : any = DBConfig().sequelize;
const localStrategy = passportLocal.Strategy;
const jwtStrategy = passportJwt.Strategy;
const extractJWT = passportJwt.ExtractJwt;

interface UserAttributes {
   id: number;
   username: string;
   password: string;
   token: string;
}

class User extends Model implements UserAttributes {
  id: number;
  username: string;
  password: string;
  token: string;

  public isValidPassword !: any;

  public static associations: {

  };

}

User.init({
  id: {
    type: DataTypes.INTEGER.UNSIGNED,
    autoIncrement: true,
    primaryKey: true,
  },
  username: {
    type: DataTypes.STRING,
    allowNull: false,
    unique: true
  },
  password: DataTypes.STRING,
  token: DataTypes.TEXT
}, {
  sequelize,
  modelName: 'User',
});

export const initPassport = () => {

  passport.use('signup', new localStrategy({
    usernameField : 'username',
    passwordField: 'password'
  }, async (username, password, done) => {

    const hash = await bcrypt.hash(password, 10);;

    console.log("username" + username);
    try{
      const newUser = await User.create({
        username,
        password: hash
      });

      console.log(newUser);


      return done(null, newUser);
    }catch(error){
      console.log(error);
      done(error);
    }

  }));

  passport.use('login', new localStrategy({
    usernameField : 'username',
    passwordField: 'password'
  },async (username, password, done) => {

    try{
        const user = await User.findOne({ where: { username } });

        if (!user) {
          return done(null, false, { message: 'User not found' });
        }

        const validate = await isValidPassword(username, password);

        if (!validate) {
          return done(null, false, { message: 'Wrong Password' });
        }

        const body = { _id: user.id, username: user.username, expiry: Date.now() + 3600 };
        const token = jwt.sign({ user: body }, 'x!#uizxuinATxxO');

        await updateToken(user.username, token);

        return done(null, token);

    } catch (error) {
      console.log(error);
        return done(error);
    }

  }));

  passport.use(
    new jwtStrategy(
      {
        secretOrKey: 'x!#uizxuinATxxO',
        jwtFromRequest: extractJWT.fromAuthHeaderAsBearerToken(),
      },
      async (payload, done) => {

         /*console.log(payload);
        const tokenExpired = await isTokenExpired(payload.user);
        */
        try {
          /*if(tokenExpired){
          console.log(tokenExpired);

            done("Token expired");
          }
          else*/
            return done(null, payload.user);
        } catch (error) {
          done(error);
        }
      }
    )
  );
  }

const isValidPassword = async (username: string, password: string) => {
  const foundUser = await User.findOne({ where: { username } });
  const compare = await bcrypt.compare(password, foundUser.password);
  return compare;
}

export const isTokenExpired = async (username: string) => {

  console.log( username);

  const foundUser = await User.findOne({ where: { username } });

  console.log(foundUser);

  const expiry = jwt.decode(foundUser.token, {complete: true}).payload.expiry;

  console.log(jwt.decode(foundUser.token, {complete: true}).payload);

  return Date.now() >= expiry;
}

const updateToken = async (username: string, token: string) => {
  const updatedUser = await User.update({ token }, {where: { username }});

  return updatedUser;
}