import * as fs from 'fs';
import { join, basename as bn, dirname } from 'path';
import { Sequelize, DataTypes } from "sequelize";

// const moduleURL = new URL(import.meta.url);
// const dirName = dirname(moduleURL.pathname);
const basename : string = bn(__filename);
const env : string = process.env.NODE_ENV || 'development';
// const config : any = require(dirName + '/../config/config.json')[env];
import config from '../config/config.json'
const db : any = {};

let sequelize : any;

export default function DBConfig() {
  if (config[env].use_env_variable) {
    sequelize = new Sequelize(process.env[config[env].use_env_variable], config[env]);
  } else {
    sequelize = new Sequelize(config[env].database, config[env].username, config[env].password, config[env]);
  }

  fs
    .readdirSync(__dirname)
    .filter(file => {
      return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.ts');
    })
    .forEach(file => {
      const model = require(join(__dirname, file))(sequelize, DataTypes);
      db[model.name] = model;
    });

  Object.keys(db).forEach(modelName => {
    if (db[modelName].associate) {
      db[modelName].associate(db);
    }
  });

  db.sequelize = sequelize;
  db.Sequelize = Sequelize;

  return db;
}