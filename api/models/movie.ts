import { Model, DataTypes } from "sequelize";

import DBConfig from './'
import { addMissingMovies } from '../util/autoFetchMovies'

const sequelize : any = DBConfig().sequelize;

interface MovieAttributes {
   id: number;
   title: string;
   description: string | null;
   thumbnail: string;
   releasedDate: string;
}

class Movie extends Model implements MovieAttributes {
  public id: number;
  public title: string;
  public description: string | null;
  public thumbnail: string;
  public releasedDate: string;

  public findMovieById!: any;
  public createNewMovie!: any;
  public updateMovieDescription!: any;
  public deleteMovie!: any;

  public static associations: {

  };

}

Movie.init({
  id: {
    type: DataTypes.INTEGER.UNSIGNED,
    autoIncrement: true,
    primaryKey: true,
  },
  title: {
    type: DataTypes.STRING,
    allowNull: false,
    unique: true
  },
  description: DataTypes.TEXT,
  thumbnail: DataTypes.STRING,
  releasedDate: DataTypes.DATE
}, {
  sequelize,
  modelName: 'Movie',
});


export const createNewMovie = async (movie: Movie) => {
  const newUser = await Movie.create({
    title: movie.title,
    description: movie.description,
    thumbnail: movie.thumbnail,
    releasedDate: movie.releasedDate
  });

  return newUser;

}

export const findMovieById = async (id: string) => {
  const foundMovie = await Movie.findOne({ where: { id } });
  if (foundMovie === null) return;
  return foundMovie;
}

export const findAllMovies = async () => {
  const foundMovie = await Movie.findAll();
  if (foundMovie === null) return;
  return foundMovie;
}

export const updateMovieDescription = async (movie: Movie) => {
  const updatedMovie = await Movie.update({ description: movie.description }, {where: { title: movie.title }});
  return updatedMovie;
}

export const deleteMovie = async (id: string) => {
  const deletedMovie = await Movie.destroy({where: { id }});
  return deletedMovie;
}

export const bulkUpsert = async () => {

  const newMovies =  await addMissingMovies();

  Movie.beforeBulkCreate((movies, options) => {

    for (const movie of movies) {
      for (const popularMovie of newMovies) {
        if (movie.title === popularMovie.title && movie.description !== popularMovie.description) {
          movie.description = popularMovie.description;
        }
      }
    }

    if (options.updateOnDuplicate && !options.updateOnDuplicate.includes('description')) {
      options.updateOnDuplicate.push('description');
    }
  });

  try{
    const result = await Movie.bulkCreate(newMovies, {
      updateOnDuplicate: ['title'],
      individualHooks: true
    });

    return { message: "Movies Inserted/Updated" , count : result.filter(movie =>  typeof movie.getDataValue("id") !== 'undefined').length, status: "SUCCESS" };


  } catch (error) {
    console.log(error);
    return { message : error.parent.text, count: 0, status: "ERROR" };
  };

}