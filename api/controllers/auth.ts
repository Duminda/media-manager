import express from "express";
import passport from "passport";
import jwt from 'jsonwebtoken';

import {initPassport, isTokenExpired} from '../models/user'

export const loginRouter = express.Router();

initPassport();

loginRouter.post(
  '/login',
  async (req, res, next) => {
    passport.authenticate(
      'login',
      async (err, user, info) => {
        try {
          if (err || !user) {
            res.sendStatus(401);
          }
          req.login(
            user,
            { session: false },
            async (error) => {
              if (error) return next(error);

              const body = { _id: user._id, username: user.username };
              const token = jwt.sign({ user: body }, 'x!#uizxuinATxxO');

              return res.json({ token });
            }
          );
        } catch (error) {
          return next(error);
        }
      }
    )(req, res, next);
  }
);

loginRouter.post("/register",
    passport.authenticate('signup', { session: false }),
    async (req, res, next) => {
    res.json({
        message: 'Registration successful'
    });
});

loginRouter.post("/introspect", async (req, res) => {

    const token = req.body.token;

    const tokenExpired = await isTokenExpired(token);

    res.json({ active: tokenExpired });
});