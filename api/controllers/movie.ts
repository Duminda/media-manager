
import express from "express";

import { createNewMovie, findAllMovies ,findMovieById, updateMovieDescription , deleteMovie, bulkUpsert} from '../models/movie'
import { initPassport } from '../models/user'

export const movieRouter = express.Router();

initPassport();

movieRouter.post("/", async (req, res) => {

    const movie = req.body;

    const created = await createNewMovie(movie);

    res.send(created);
});

movieRouter.get("/:id", async (req, res) => {

    const movie = await findMovieById(req.params.id);

    res.send(movie);
});

movieRouter.get("/", async (req, res) => {

    const movies = await findAllMovies();

    res.send(movies);
});

movieRouter.put("/", async (req, res) => {

    const isUpdated  = await updateMovieDescription(req.body);

    res.send(isUpdated[0] === 1 ? req.body : {});
});

movieRouter.delete("/:id", async (req, res) => {

    const isDeleted = await deleteMovie(req.params.id);

    res.sendStatus(isDeleted === 1 ? 204 : 404)
});

movieRouter.post("/bulk", async (req, res) => {

    const bulkCreated = await bulkUpsert();

    res.send(bulkCreated);
});
