import express from "express";
import bodyParser from "body-parser"
import passport from 'passport'
import cors from 'cors'


import  { movieRouter } from '../controllers/movie'
import  { loginRouter } from '../controllers/auth'

const app : any = express();
const port : number = 3000;

app.use(bodyParser.json());
app.use(passport.initialize());

const allowedOrigins = ['*'];

const options: cors.CorsOptions = {
  origin: allowedOrigins
};

app.use(cors(options));

app.use('/movies', passport.authenticate('jwt', { session: false }), movieRouter);
app.use('/auth', loginRouter);

app.listen(port, ()=>{
    console.log(`Movie API started at port : ${port}`);
});

