#!/bin/sh

>&2 echo "Check DB!"
while ! mysqladmin ping -h mariadb -uroot -proot; do
    >&2 echo "Wait ..."
    sleep 5
done
>&2 echo "DB ready!"

npx sequelize-cli db:migrate
npx sequelize-cli db:seed:undo:all
npx sequelize-cli db:seed:all

npm start