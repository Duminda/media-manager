import axios from 'axios'
import popularMovies from "../resources/popularMovies.json"

const getAllPopularMovies : any = async () => {

    const allMovies = await axios.get( 'https://api.themoviedb.org/3/movie/popular?api_key=0ade51acab19d038bdd32b3d2b1704a3&language=en-US')

    return allMovies.data.results.map(movie => {
        return {
            title: movie.original_title,
            description: movie.overview,
            thumbnail: movie.poster_path,
            releasedDate: movie.release_date
        }
    })

}

export const addMissingMovies : any = async () => {

     const allPopularMovies = await getAllPopularMovies();

     const newPopularMovies = [];

     allPopularMovies.find((movie, index) => {
        if(!popularMovies.includes(movie)){
            newPopularMovies.push(movie);
        }
     });

     return [...newPopularMovies, ...popularMovies];

}