'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {

    await queryInterface.bulkInsert('Movies', [{
        title: 'Machang',
        description: 'bla.. blaa..',
        thumbnail: '/1g1dhYtq4irTY1GPXvft6k4LLjm.jpg',
        releasedDate: '2021-11-27',
        createdAt: '2021-12-17',
        updatedAt: '2021-12-17'
    }], {});
   
  },

  down: async (queryInterface, Sequelize) => {

    await queryInterface.bulkDelete('Movies', null, {});
    
  }
};
