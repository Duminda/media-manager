'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {

    //--  user: demouser/  pass: abc123  --
    await queryInterface.bulkInsert('Users', [{
        username: 'demouser',
        password: '$2b$10$NpMDdFu9PhesDoWtL3GG2.fbvSSpDjbyWODzsJfuoxwnW4Qzuey8m',
        createdAt: '2021-12-17',
        updatedAt: '2021-12-17'
    }], {});
   
  },

  down: async (queryInterface, Sequelize) => {

    await queryInterface.bulkDelete('Users', null, {});
    
  }
};
